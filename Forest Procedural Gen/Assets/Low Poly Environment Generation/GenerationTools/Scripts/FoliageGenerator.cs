﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// [System.Serializable]
// public enum FoliageType {
//     Tree,
//     Grass,
//     Flowers,
//     Rocks
// }

[System.Serializable]
public class FoliageProperies {
    [Header ("Foliage Type")]
    [Tooltip ("Give this layer a name")]
    public string foliageType;

    [Header ("Foliage GameObject")]
    [Tooltip ("Add different objects here to be scattered on this layer")]
    public List<GameObject> objectPrefabs = new List<GameObject> ();

    [Header ("Foliage Parent Category")]
    [Tooltip ("Make a new GameObject, under the mesh gameobject to host all prefabs for this layer")]
    public Transform foliageParent;

    [Header ("Spawn Height")]
    [Tooltip ("Lowest Spawn Point")]
    public float minimumSpawnHeight;
    [Tooltip ("Highest Spawn Point")]
    public float maximumSpawnHeight;

    [Header ("Clumping")]
    [Tooltip ("The lower the value, the more clumping will occur")]
    [Range (0, 30)]
    public int clumpDensity;

    [Header ("FoliageSize")]
    [Tooltip ("Object min Height")]
    [Range (0, 1)]
    public float foliageMinHeight;
    [Tooltip ("Object max Height")]
    [Range (0, 1)]
    public float foliageMaxHeight;

    [Header ("Spawn Object")]
    [Tooltip ("If selected, objects will spawn")]
    public bool spawnObject;

    [Header ("Destroy On Generate")]
    [Tooltip ("Destroy object layer when layer is generator")]
    public bool destroyOnGenerate;
}

public class FoliageGenerator : MonoBehaviour {
    [HideInInspector] public List<Vector3> vertexData = new List<Vector3> ();
    public static FoliageGenerator instance;

    [Header ("Foliage Regions")]
    public List<FoliageProperies> foliageData = new List<FoliageProperies> ();

    public void SpawnTree () {
        ClearTrees ();
        foreach (var item in foliageData) {
            if (item.spawnObject) {

                for (int i = 0; i < vertexData.Count; i++) {
                    int chance = Random.Range (0, (item.clumpDensity));
                    Vector3 vertexPoint = vertexData[i];
                    GameObject selectedTree = item.objectPrefabs[Random.Range (0, item.objectPrefabs.Count)].gameObject;
                    if (vertexPoint.y > item.minimumSpawnHeight && vertexPoint.y < item.maximumSpawnHeight && chance == 0) {
                        var newTree = Instantiate (selectedTree, vertexPoint * 10, Quaternion.identity);
                        newTree.transform.localEulerAngles = new Vector3 (0, Random.Range (-360, 360), 0);
                        newTree.transform.SetParent (item.foliageParent);
                        newTree.transform.localScale = new Vector3 (newTree.transform.localScale.x, Random.Range (item.foliageMinHeight, item.foliageMaxHeight), newTree.transform.localScale.z);
                        if (newTree.GetComponent<FoliageHeightCorrection> () != null) {
                            newTree.GetComponent<FoliageHeightCorrection> ().CorrectHeight ();
                        }
                    }
                }
            }
        }
    }

    public void ClearTrees () {
        foreach (var item in foliageData) {
            if (item.destroyOnGenerate) {
                for (int i = item.foliageParent.childCount - 1; i > -1; i--) {
                    DestroyImmediate (item.foliageParent.GetChild (i).gameObject);
                }
            }
        }
    }
}