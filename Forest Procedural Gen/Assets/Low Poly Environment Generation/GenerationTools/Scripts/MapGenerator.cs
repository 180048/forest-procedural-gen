﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DrawMode {
    NoiseMap,
    ColourMap,
    Mesh,
    Terrain
}

[System.Serializable]
public struct TerrainType {
    public string name;
    public float height;
    public Color colour; // change out for texture later
}

public class MapGenerator : MonoBehaviour {
    public DrawMode drawMode;
    [Header ("Map Size")]
    public int mapWidth;
    public int mapHeight;
    [Header ("Noise Settings")]
    [Tooltip ("Scale of the entire noise map")]
    public float noiseScale;
    public int octaves;
    [Range (0, 1)]
    [Tooltip ("Opacity of aditional layer of noise between 0 and 1, Creates bumpiness in terrain")]
    public float persistance;
    [Tooltip ("Level or detail of aditional noise")]
    public float lacunarity;
    [Tooltip ("Each number here yields its own terrain style")]
    public int seed;

    // public Vector2 offset;
    [Header ("Mesh Settings")]
    [Tooltip ("Height multiplier")]
    public float meshHeightMultiplier;
    [Tooltip ("Use this to make flatter ground but taller rocks")]
    public AnimationCurve meshHeightCurve;

    [Header ("Water Plane Settings")]
    public GameObject waterPlane;

    [Header ("Auto Update In Inspector")]
    [Tooltip ("Enable this to see effects happen in realtime")]
    public bool autoUpdate;
    [Tooltip ("Regions to set the colours of different heights")]
    public List<TerrainType> regions = new List<TerrainType> ();

    public void GenerateMap () {
        float[, ] noiseMap = Noise.GenerateNoiseMap (mapWidth, mapHeight, seed, noiseScale, octaves, persistance, lacunarity);

        Color[] colourMap = new Color[mapWidth * mapHeight];
        if (waterPlane != null) {
            waterPlane.transform.localScale = new Vector3 (mapWidth * 10, 1, mapHeight * 10);
            waterPlane.transform.position = new Vector3 (0, regions[0].height * 5, 0);
        }

        for (int y = 0; y < mapHeight; y++) {
            for (int x = 0; x < mapWidth; x++) {
                float curHeight = noiseMap[x, y];
                for (int i = 0; i < regions.Count; i++) {
                    if (curHeight <= regions[i].height) {
                        colourMap[y * mapWidth + x] = regions[i].colour;
                        break;
                    }
                }
            }
        }
        FoliageGenerator foliageGenerator = GetComponent<FoliageGenerator> ();
        MapDisplay display = FindObjectOfType<MapDisplay> ();
        if (drawMode == DrawMode.NoiseMap) {
            display.DrawTexture (TextureGenerator.TextureFromHeightMap (noiseMap));
        } else if (drawMode == DrawMode.ColourMap) {
            display.DrawTexture (TextureGenerator.TextureFromColourMap (colourMap, mapWidth, mapHeight));
        } else if (drawMode == DrawMode.Mesh) {
            display.DrawMesh (MeshGenerator.GenerateTerrainMesh (noiseMap, meshHeightMultiplier, meshHeightCurve, foliageGenerator), TextureGenerator.TextureFromColourMap (colourMap, mapWidth, mapHeight));
            // } else if (drawMode == DrawMode.Terrain) {
            //     display.DrawTerrain (TextureGenerator.;
            //     }
        }
    }

    void OnValidate () {
        if (mapWidth < 1) {
            mapWidth = 1;
        }
        if (mapHeight < 1) {
            mapHeight = 1;
        }
        if (lacunarity < 1) {
            lacunarity = 1;
        }
        if (octaves < 0) {
            octaves = 1;
        }
        //reset value here so that it remains to the power of 2
    }

}