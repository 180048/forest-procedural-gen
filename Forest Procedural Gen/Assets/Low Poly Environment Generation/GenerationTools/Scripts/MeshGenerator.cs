﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MeshGenerator {

    public static MeshData GenerateTerrainMesh (float[, ] heightMap, float heightMultiplier, AnimationCurve heightCurve, FoliageGenerator _foliageGenerator) {

        int width = heightMap.GetLength (0);
        int height = heightMap.GetLength (1);
        float topLeftX = (width - 1) / -2;
        float topLeftZ = (height - 1) / 2f;
        // GenerateFoliage foliageGenerator = new GenerateFoliage ();

        MeshData meshData = new MeshData (width, height);
        // GenerateTrees generateTrees = new GenerateTrees ();
        int vertexIndex = 0;

        _foliageGenerator.vertexData.Clear ();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                meshData.vertices[vertexIndex] = new Vector3 (topLeftX + x, heightCurve.Evaluate (heightMap[x, y]) * heightMultiplier, topLeftZ - y);
                meshData.uvs[vertexIndex] = new Vector2 (x / (float) width, y / (float) height);
                _foliageGenerator.vertexData.Add (new Vector3 (topLeftX + x, heightCurve.Evaluate (heightMap[x, y]) * heightMultiplier, topLeftZ - y));
                // foliageGenerator.vertexData.Add (new Vector3 (topLeftX + x, heightCurve.Evaluate (heightMap[x, y]) * heightMultiplier, topLeftZ - y));

                // if (_spawnTrees) {
                //     //spawn trees here
                //     int chance = Random.Range (0, 6);
                //     float heightSize = heightCurve.Evaluate (heightMap[x, y]) * heightMultiplier;
                //     if (heightSize > 0.5f && heightSize < 4 && chance == 2) {
                //         var newTree = GenerateFoliage.Instantiate (tree, new Vector3 (topLeftX + x * 10, (heightCurve.Evaluate (heightMap[x, y]) * heightMultiplier) * 10, topLeftZ - y * 10), Quaternion.identity);
                //         newTree.transform.localEulerAngles = new Vector3 (0, Random.Range (-360, 360), 0);
                //         newTree.transform.SetParent (treeParent);
                //     }
                // }

                // Debug.Log ($"Vertex distance is { heightCurve.Evaluate (heightMap[x, y]) * heightMultiplier}");

                if (x < width - 1 && y < height - 1) {
                    meshData.AddTriangle (vertexIndex, vertexIndex + width + 1, vertexIndex + width);
                    meshData.AddTriangle (vertexIndex + width + 1, vertexIndex, vertexIndex + 1);
                }
                vertexIndex++;
            }
        }
        return meshData;
    }
}

public class GenerateTrees {
    public void SpawnTrees (bool _spawnTrees, GameObject tree, Transform treeParent) {
        // GenerateFoliage foliageGenerator = new GenerateFoliage ();
        // for (int i = 0; i < foliageGenerator.vertexData.Count; i++) {
        // spawn trees here
        // Vector3 vertexPointData = foliageGenerator.vertexData[i];
        // Debug.Log ($"Tree spawned at {vertexPointData}");
        // int chance = Random.Range (0, 6);
        // Vector3 vertexPointData = foliageGenerator.vertexData[i];
        // if (vertexPointData.y > 0.5f && vertexPointData.y < 4 && chance == 2) {
        //     var newTree = GenerateFoliage.Instantiate (tree, new Vector3 (topLeftX + x * 10, (heightCurve.Evaluate (heightMap[x, y]) * heightMultiplier) * 10, topLeftZ - y * 10), Quaternion.identity);
        //     newTree.transform.localEulerAngles = new Vector3 (0, Random.Range (-360, 360), 0);
        //     newTree.transform.SetParent (treeParent);
        // }
        // }
    }
}

public class MeshData {
    public Vector3[] vertices;
    public int[] triangles;
    public Vector2[] uvs;
    int triangleIndex;
    public MeshData (int meshWidth, int meshHeight) {
        vertices = new Vector3[meshWidth * meshHeight];
        uvs = new Vector2[meshWidth * meshHeight];
        triangles = new int[(meshWidth - 1) * (meshHeight - 1) * 6];
    }

    public void AddTriangle (int a, int b, int c) {
        triangles[triangleIndex] = a;
        triangles[triangleIndex + 1] = b;
        triangles[triangleIndex + 2] = c;
        triangleIndex += 3;
    }

    public Mesh CreateMesh () {
        Mesh mesh = new Mesh ();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;
        mesh.RecalculateNormals ();
        return mesh;
    }
}

// public class GenerateFoliage : MonoBehaviour {
//     public List<Vector3> vertexData = new List<Vector3> ();
// }