﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoliageHeightCorrection : MonoBehaviour {

    [SerializeField] float distance;
    [SerializeField] LayerMask layer;
    public void CorrectHeight () {
        // Debug.Log ("ShotOut");
        RaycastHit hit;
        if (Physics.Raycast (transform.position + ((Vector3.up) * 3), Vector3.down, out hit, distance, layer)) {
            transform.position = new Vector3 (transform.position.x, hit.point.y, transform.position.z);
            Debug.Log ("HeightCorrected");
        }
    }
    // private void Update () {
    //     RaycastHit hit;
    //     if (Physics.Raycast (transform.position + ((Vector3.up) * 50), Vector3.down, out hit, distance)) {

    //         Debug.Log ("hit");
    //     }
    // }
}