﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour {

    Terrain terrain;

    private void Start () {
        terrain = GetComponent<Terrain> ();
        // terrain.terrainData = GenerateTerrainData (terrain.terrainData);
    }

    public void GenerateTerrain (float[, ] heightMap) {

        int width = heightMap.GetLength (0);
        int height = heightMap.GetLength (1);
        float topLeftX = (width - 1) / -2;
        float topLeftZ = (height - 1) / 2f;

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                terrain.terrainData.SetHeights ((int) topLeftX + x, (int) topLeftZ - y, heightMap);
            }
        }
    }

    // TerrainData GenerateTerrainData (TerrainData terrainData) {
    //     terrainData.SetHeights (0, 0, )
    // }
}
// meshData.vertices[vertexIndex] = new Vector3 (topLeftX + x, heightCurve.Evaluate (heightMap[x, y]) * heightMultiplier, topLeftZ - y);
// meshData.uvs[vertexIndex] = new Vector2 (x / (float) width, y / (float) height);
// vertexIndex++;