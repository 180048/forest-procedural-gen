﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapDisplay : MonoBehaviour {
    public Renderer texRend;
    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;

    public void DrawTexture (Texture2D texture) {
        texRend.sharedMaterial.SetTexture ("_BaseMap", texture);
        texRend.transform.localScale = new Vector3 (texture.width, 1, texture.height);
    }
    public void DrawMesh (MeshData meshData, Texture2D _texture) {
        meshFilter.sharedMesh = meshData.CreateMesh ();
        meshRenderer.sharedMaterial.mainTexture = _texture;
    }
    // public void DrawTerrain (float[,] noiseHeight) {
    //     var terrainScript = terrain.GetComponent<TerrainGenerator> ();
    //     terrainScript.GenerateTerrain (_texture);
    //     // var terrains = GetComponent<Terrain>();
    //     // terrains.terrainData.
    // }
}