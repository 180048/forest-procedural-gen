﻿using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (FoliageGenerator))]
public class FoliageGeneratorEditor : Editor {
    public override void OnInspectorGUI () {
        FoliageGenerator folGen = (FoliageGenerator) target;

        if (DrawDefaultInspector ()) {
            // if (folGen.autoUpdate) {
            //     mapGen.GenerateMap ();
            // }
        }
        if (GUILayout.Button ("Generate")) {
            folGen.SpawnTree ();
        }
        if (GUILayout.Button ("ClearTrees")) {
            folGen.ClearTrees ();
        }
    }
}